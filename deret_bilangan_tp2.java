import java.util.Scanner;

public class deret_bilangan_tp2 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		String loop = "y";
		
		while(loop.equals("y")){
		
			int min_bilangan = 2, max_bilangan = 10, min_beda = 2, max_beda = 9;
			int bilangan, beda, a=1, aritmatika, geometri, faktorial = 1;
			boolean task_1 = false, task_2 = false;
			
			//validasi inputan bilangan
			do {
				System.out.print("Masukkan banyak bilangan yang mau di cetak [2..10]: ");
				bilangan = input.nextInt();
				if (bilangan >= min_bilangan && bilangan <= max_bilangan) {
					task_1 = true;
				}
				else {
					System.out.println("Banyak bilangan harus mininal 2 dan maksimal 10");
				}
			} while (!task_1);
			
			//validasi inputan beda
			do {
				System.out.print("Masukkan beda masing-masing angka [2..9]: ");
				beda = input.nextInt();
				if (beda >= min_beda && beda <= max_beda) {
					task_2 = true;
				}
				else {
					System.out.println("Beda harus harus mininal 2 dan maksimal 9");
				}
			} while (!task_2);
			
			
			//Perhitungan Deret Aritmatika
			System.out.println("Deret Aritmatika");
			for(int i = 1; i <= bilangan; i++) {
			
				aritmatika = a + (i-1) * beda;		
				System.out.print(aritmatika +" ");
			}
			System.out.println();
			System.out.println();
			
			//Perhitungan Deret Geometri
			System.out.println("Deret Geometri");
			for (int i = 1; i <= bilangan; i++) {
				
				int pangkat_rasio = (int) Math.pow(beda, (i-1));
				geometri = a * pangkat_rasio;
				System.out.print(geometri +" ");
			}
			System.out.println();
			System.out.println();
			
			//Perhitungan Faktorial
			System.out.println("Faktorial dari "+ bilangan);
			for (int i = bilangan; i >= 1; i--) {
				if (i != 1) {
					System.out.print(i+ " * ");
				} else {
					System.out.print(i);
				}
	
				faktorial *= i;
			}
			System.out.print(" = " + faktorial);
			System.out.println();
			
			
			//ulang program
			System.out.println();
			System.out.print("Anda Ingin Mengulang [y/t]: ");
            loop = input.next().toLowerCase();
		}
		
		System.out.println("bye..");
		

	}

}
